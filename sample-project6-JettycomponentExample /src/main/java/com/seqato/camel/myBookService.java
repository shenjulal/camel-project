package com.seqato.camel;

import org.apache.camel.Processor;
import org.apache.camel.Exchange;



public class myBookService implements Processor {
    public void process(Exchange exchange) throws Exception {
        
    	String bookId = (String) exchange.getIn().getHeader("bookid");
        System.out.println("Retrieving Book...");
        exchange.getOut().setBody("<html><body>Book " + bookId + " The Immortals of Meluha.</body></html>");
    }
}