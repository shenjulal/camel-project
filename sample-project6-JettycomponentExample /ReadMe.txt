Camel Router Spring Project
===========================

To build this project use

    mvn install

To run this project with Maven use

    mvn camel:run
    
    Please check this link for response :  http://localhost:8888/myBookService?bookid=1001

For more help see the Apache Camel documentation

    http://camel.apache.org/


    
 