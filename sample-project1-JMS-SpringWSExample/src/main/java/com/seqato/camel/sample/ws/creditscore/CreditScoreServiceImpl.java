package com.seqato.camel.sample.ws.creditscore;

import org.springframework.stereotype.Service;

@Service(value="creditScoreService")
public class CreditScoreServiceImpl implements CreditScoreService {

	@Override
	public int getCreditScore(long accountNumber) {
		if(accountNumber == 123) {
			return 756;
		}else if(accountNumber == 456) {
			return 263;
		}else {
			return 378;
		}
	}

}
