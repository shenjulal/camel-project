package com.seqato.camel.sample.client;

import org.apache.camel.util.IOHelper;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.seqato.camel.sample.ws.creditscore.CreditScoreService;

/**
 * Client that uses Camel Spring Remoting for very easy integration with the server.
 * <p/>
 * Requires that the JMS broker is running, as well as CamelServer
 */
public final class CamelClientRemoting {
    private CamelClientRemoting() {
        //Helper class
    }

    // START SNIPPET: e1
    public static void main(final String[] args) {
        System.out.println("Notice this client requires that the CamelServer is already running!");

        AbstractApplicationContext context = new ClassPathXmlApplicationContext("camel-client-remoting.xml");
        // just get the proxy to the service and we as the client can use the "proxy" as it was
        // a local object we are invoking. Camel will under the covers do the remote communication
        // to the remote ActiveMQ server and fetch the response.
        CreditScoreService creditScoreService = context.getBean("creditScoreProxy", CreditScoreService.class);

        System.out.println("Invoking getCreditScore service with 456 expected result is 263");
        int response = creditScoreService.getCreditScore(456);
        System.out.println("... the result is: " + response);

        // we're done so let's properly close the application context
        IOHelper.close(context);
    }

}
