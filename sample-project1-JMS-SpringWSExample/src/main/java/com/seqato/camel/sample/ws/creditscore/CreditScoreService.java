package com.seqato.camel.sample.ws.creditscore;

public interface CreditScoreService {
	int getCreditScore(long accountNumber);
}
