package com.seqato.camel.sample.route;

import org.apache.camel.builder.RouteBuilder;

public class ServerRoutes extends RouteBuilder {
	 
    @Override
    public void configure() throws Exception {
        
        from("jms:queue:numbers").to("bean:creditScoreService");
 
      
    }
 
}