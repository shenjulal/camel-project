package com.seqato.camelsample.Rest;


import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;


public class CustomerServiceRoute extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        getContext().setTracing(true);


//        from("cxfrs:bean:rsServer")
        from("cxfrs://http://localhost:9090/route?resourceClasses=com.seqato.camelsample.Rest.CustomerServiceResource")
                .setHeader(Exchange.FILE_NAME, simple("test-${body}.xml"))
//                .pollEnrich("file:src/data?noop=true", 1000, new CustomerEnricher())
                .process(new CustomerServiceProcessor())
                .log("Here is the message that was enriched: ${body}");
    }
}
