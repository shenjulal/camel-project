Camel Router Spring Project
===========================

To build this project use

    mvn install

To run this project with Maven use

    mvn camel:run
    
    Please check this link for response : http://localhost:9090/route/customerservice/customers/108

For more help see the Apache Camel documentation

    http://camel.apache.org/

