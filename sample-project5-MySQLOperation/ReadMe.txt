Camel Router Spring Project
===========================

Configure the project using following tips

	Create database in MySQL with name= "test"
	
	Configure the database using camel-context.xml file
	
	Create the Table "ARTICLES" using the file "db-schema.sql" from the folder src/main/resources 
	
	Add the test data from file "db-test-data.sql" from the folder src/main/resources  

To build this project use

    mvn install

To run this project with Maven use

    mvn camel:run

For more help see the Apache Camel documentation

    http://camel.apache.org/
    

